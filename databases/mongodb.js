const Mongoose = require("mongoose"),
      Config = require('config');

exports.init = function () {

  let connection_uri = `mongodb://${Config.database.mongo.host}:${Config.database.mongo.port}/${Config.database.mongo.name}`;
  Mongoose.connect(connection_uri, {
      reconnectTries: Number.MAX_VALUE,
      reconnectInterval: 1000 });

      Mongoose.connection.on('connected', function () {
      console.log('Mongoose connected ', connection_uri);
  });

  Mongoose.connection.on('error', function (err) {
	  console.log('Mongoose connection error: ' + err);
  });


  Mongoose.connection.on('disconnected', function () {
	  console.log('Mongoose connection disconnected');
  });


  process.on('SIGINT', function () {
    Mongoose.connection.close(function () {
			console.log('Mongoose default connection disconnected through app termination');
          	process.exit(0);
      });
  });
};

exports.close = async () => {
  return await Mongoose.connection.close();
};
