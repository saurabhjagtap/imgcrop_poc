const sharp = require('sharp');

sharp('./3.jpeg').metadata()
.then(function(metadata) {
	console.log(metadata.width, metadata.height)
   if (metadata.width > metadata.height){
	   sharp('./3.jpeg')
	   .resize(Math.floor(metadata.height * (3/2)), metadata.height)
	   .toFile('3_new.jpeg')
	   .then(() => sharp('./3_new.jpeg').metadata()
	   	.then((metadata) => console.log("done if",metadata.width, metadata.height)))
   }
   else {
	sharp('./3.jpeg')
	   .resize(metadata.width, Math.floor(metadata.width * (2/3)))
	   .toFile('3_new.jpeg')
	   .then(() => sharp('./3_new.jpeg').metadata()
	   	.then((metadata) => console.log("done if",metadata.width, metadata.height)))
   }
});