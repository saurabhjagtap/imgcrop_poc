const mongoose = require('mongoose');
const __MODEL__ = require("src/models/model");


exports.find = (condition, projection) => {
	return new Promise(function(resolve, reject) {
		__MODEL__.find(condition, projection, function(err, result) {
			if (err) {
				reject(err);
				console.log(err)
			}

			resolve(result);
		});
	});
};
