const Interface = require("src/interfaces/interface"),
  Response = require("src/utils/response"),
	StatusCodes = require("src/utils/status_codes"),
	ResponseMessages = require("src/utils/response_messages"),
	Constants = require("src/utils/constants"),
	Utils = require("src/utils/utils"),
  EndPoints = require("src/utils/end_points"),
  fs = require('fs'),
	to = require('src/utils/promise_handler'),
  sharp = require('sharp');

exports.rootRequest = async (request, h) => {
	let err = null, imgObj = null, metadata = null, result;
  let condition = {"id": request.params.id};
  
  //find image details in db
  [err, imgObj] = await to(Interface.find(condition));
	if (err || imgObj == null)  return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
  
  //find current image resolution
  [err, metadata] = await to(sharp(imgObj[0].path).metadata())
  if (err)  return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);

  //check if image is already in required resoltion
  if (Math.round(((metadata.width / metadata.height) + Number.EPSILON) * 10)/10 == 1.5)
    return h.file(imgObj[0].path)
  console.log(Math.round(((metadata.width / metadata.height) + Number.EPSILON) * 10)/10)

  //check what is the smaller dimension of the image (width or height)
  if (metadata.width > metadata.height){

    // if height is smaller, reduce the width, and crop image from center.
    [err, result] = await to(
      sharp(imgObj[0].path)
			.resize(Math.floor(metadata.height * (3/2)), metadata.height)
      .toBuffer());

    if (err){
      console.log("error in if: ", err)
      return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
    }
    
    //we write the image data in buffer and then write to file
    //because we cant directly overwrite the input file  
    [err, result] = await to(sharp(result).toFile(imgObj[0].path))
    //console.log(result)

    if (err){
      console.log("error in if: ", err)
      return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
    }  
    return h.file(imgObj[0].path)
  }
  // else, reduce the height, and crop image from center.
  else {
    [err, result] = await to(
      sharp(imgObj[0].path)
      .resize(metadata.width, Math.floor(metadata.width * (2/3)))
      .toBuffer()
      );
      
    [err, result] = await to(sharp(result).toFile(imgObj[0].path))
      
    console.log(result)
    if (err) {
      console.log("error in else: ", err)
      return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
    }
    return h.file(imgObj[0].path)
  }
}

  /*
  metadata.width > metadata.height ? (newWidth = Math.floor(metadata.height * (3/2)), newHeight = metadata.height) : (newWidth = metadata.width, newHeight = Math.floor(metadata.width * (2/3)))
  console.log(newWidth, newHeight)
  [err, result] = await to(sharp(imgObj[0].path).resize(newWidth, newHeight).toBuffer());
  if (err) {
    console.log(err)
    return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
  }

  [err, result] = await to(sharp(result).toFile(imgObj[0].path))
  if (err) {
    console.log(err)
    return h.response(Response.sendResponse(false, err, ResponseMessages.ERROR, StatusCodes.BAD_REQUEST)).code(StatusCodes.BAD_REQUEST);
  }
  return h.file(imgObj[0].path)
}
*/