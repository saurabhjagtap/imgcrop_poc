const mongoose = require('mongoose');
const schema = mongoose.Schema;

let UserSchema = new schema({ 
	id:{
		type : schema.Types.String,
		required : true
	},
	path:{
		type: schema.Types.String
	}
})

module.exports = mongoose.model("user", UserSchema, "user");

// const SpecialSchema = new schema({
// 	email:{
// 		type: String,
// 		required: true,
// 	},
// 	value:{
// 		type : String,
// 		required : true
// 	},
// 	name:{
// 		type : String,
// 		required : true
// 	},
// 	date:{
// 		type: Date,
// 		default: Date()
// 	},
// })


// module.exports = Special = mongoose.model('special', SpecialSchema);
//exports.defaultChoice = mongoose.model('defaultchoice', DefaultSchema)
//exports.User = mongoose.model('user',UserSchema);