const controller = require('src/controllers/controller');
module.exports = [
	{
		method:"GET",
		path:"/img/{id}",
		config:controller.rootRequest
	}
]