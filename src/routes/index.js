
const path = require("path"),
fs = require("fs"),
_ = require("lodash");

fs.readdirSync(__dirname).forEach(file => {

	if (file === "index.js") {
	return;
}

const mod = {};
mod[path.basename(file, ".js")] = require(path.join(__dirname, file));
_.extend(module.exports, mod);
});
