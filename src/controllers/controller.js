const Response = require("src/utils/response"),
	Factory = require("src/factory/factory"),
	Joi = require('joi');

const response_format = Joi.object({
	is_success: Joi.boolean().required(),
	result: Joi.any().optional(),
	message: Joi.string().required(),
	status_code: Joi.number().required(),
}),
response = {
	status: {
		200: response_format,
		201: response_format,
		400: response_format,
		500: response_format,
	},
};

exports.rootRequest = {
	description: "Root location",
	notes: "rootNote",
	tags: ["api"],
	plugins: {},
		handler : (request, h) => {
			return Factory.rootRequest(request, h);
	}
};
