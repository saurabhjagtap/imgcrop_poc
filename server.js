const Hapi = require("@hapi/hapi"),
Inert = require('@hapi/inert'),
	Config = require("config");
//	_plugins = require("src/plugins/index");

exports.init = async database => {
	try {
		const port = process.env.PORT || Config.server.port;
		const server = new Hapi.Server({
			debug: { request: ["error"] },
			port: port,
			routes: {
				cors: {
					origin: ["*"],
				},
			},
		});
		await server.register(Inert)
		let Routes = require("src/routes");
		for (const route in Routes) {
			console.log("### ROUTES => ", route);
			server.route(Routes[route]);
		}

		server.start();

		server.events.on("response", request => {
			if (request.response) {
				console.log(
					`${request.info.remoteAddress}: ${request.method.toUpperCase()} ${
						request.url.pathname
					} --> ${request.response.statusCode} / time: ${Date()}`,
				);
			} else {
				console.log(
					"No statusCode : ",
					request.info.remoteAddress +
						": " +
						request.method.toUpperCase() +
						" " +
						request.url.pathname +
						" --> ",
				);
			}
		});

		server.events.on("route", route => {
			console.log(`New route added: ${route.path}`);
		});
		server.events.on("start", route => {
			console.log("Node server is running on ==> ", server.info.uri);
		});
		server.events.on("stop", route => {
			console.log("Server has been stopped");
		});
		return server;
	} catch (err) {
		console.log("Error starting server: ", err);
		throw err;
	}
};
