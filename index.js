require("app-module-path").addPath(__dirname);

const Server = require("./server");

const MongoDatabase = require("./databases/mongodb");

// require('app-module-path').addPath(__dirname);
console.log(`Running environment ==> ${process.env.NODE_ENV}`);

// Catch unhandling unexpected exceptions
process.on("uncaughtException", error => {
	console.log(`uncaughtException ==> ${error.message}`);
});

// Catch unhandling rejected promises
process.on("unhandledRejection", reason => {
	console.log(`unhandledRejection ==> ${reason}`);
});

// Init Database
MongoDatabase.init();

//Start node server
Server.init();
